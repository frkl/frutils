[![PyPI status](https://img.shields.io/pypi/status/frutils.svg)](https://pypi.python.org/pypi/frutils/)
[![PyPI version](https://img.shields.io/pypi/v/frutils.svg)](https://pypi.python.org/pypi/frutils/)
[![Pipeline status](https://gitlab.com/frkl/frutils/badges/develop/pipeline.svg)](https://gitlab.com/frkl/frutils/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# frutils

Shared utility methods for python projects under the 'frkl' umbrella.


## Description

Documentation and the majority of testing still to be done. This package contains general utility methods (under ``frutils.frutils``), command-line related ones (``frutils.frutils_cli``).

It also contains 3 packages that will be re-factored out into their own libraries eventually:

- ``frutils.doc``
- ``frutils.tasks``
- ``frutils.config``


# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'frutils' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 frutils
    git clone https://gitlab.com/frkl/frutils
    cd <frutils_dir>
    pyenv local frutils
    pip install -e .[develop,testing,docs]
    pre-commit install


## Copyright & license

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)


Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

### frkl product ids

Versions:

  - 0.9:
    - 97de2bf5-0fbb-4884-9d26-488217e1477c
  - 1.x.x:  
    - 97de2bf5-0fbb-4884-9d26-488217e1477c

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
