=======
frutils
=======

This is the documentation of **frutils**.

**frutils** is a collection of utility methods that are used in `frkl <https://frkl.io>`_ Python projects, first and
foremost `freckles <https://freckles.io`_.

Code: `https://gitlab.com/frkl/frutils`_

Contents
========

.. toctree::
   :maxdepth: 2

   License <license>
   Authors <authors>
   Changelog <changelog>
   Module Reference <api/modules>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
