frutils.tasks package
=====================

Submodules
----------

frutils.tasks.callback module
-----------------------------

.. automodule:: frutils.tasks.callback
    :members:
    :undoc-members:
    :show-inheritance:

frutils.tasks.tasks module
--------------------------

.. automodule:: frutils.tasks.tasks
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: frutils.tasks
    :members:
    :undoc-members:
    :show-inheritance:
