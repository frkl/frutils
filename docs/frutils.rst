frutils package
===============

Subpackages
-----------

.. toctree::

    frutils.config
    frutils.tasks

Submodules
----------

frutils.defaults module
-----------------------

.. automodule:: frutils.defaults
    :members:
    :undoc-members:
    :show-inheritance:

frutils.doc module
------------------

.. automodule:: frutils.doc
    :members:
    :undoc-members:
    :show-inheritance:

frutils.exceptions module
-------------------------

.. automodule:: frutils.exceptions
    :members:
    :undoc-members:
    :show-inheritance:

frutils.frutils module
----------------------

.. automodule:: frutils.frutils
    :members:
    :undoc-members:
    :show-inheritance:

frutils.frutils\_cli module
---------------------------

.. automodule:: frutils.frutils_cli
    :members:
    :undoc-members:
    :show-inheritance:

frutils.jinja2\_filters module
------------------------------

.. automodule:: frutils.jinja2_filters
    :members:
    :undoc-members:
    :show-inheritance:

frutils.parameters module
-------------------------

.. automodule:: frutils.parameters
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: frutils
    :members:
    :undoc-members:
    :show-inheritance:
