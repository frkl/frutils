=========
Changelog
=========

Upcoming
========

- beta release, leading up to 1.0.0

Version 0.9
===========

- first release of *frutils*
